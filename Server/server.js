const express = require('express')

const app = express()

app.use(express.json())

const empRouter = require('./employee')

app.use(empRouter)

app.listen(4000,'0.0.0.0',()=>{
    console.log('server startd on port 4000')
})