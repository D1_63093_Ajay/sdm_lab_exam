const express = require('express')
const utils = require('./utils')
const db = require('./db')

const router = express.Router()


router.get('/showemp',(request,response)=>{

    const{ emp_name}=request.body

    const statement = `SELECT * FROM employee WHERE emp_name =?`

    db.pool.query(statement,[emp_name],(error , data)=>{
      
        response.send(utils.createResult(error,data))
    })

})

router.post('/addemp',(request,response)=>{

    const{ emp_name, emp_Address ,emp_Email,emp_MobileNo,emp_DOB, emp_JoiningDate}=request.body

    const statement = `INSERT INTO employee (emp_name, emp_Address ,emp_Email,emp_MobileNo,emp_DOB, emp_JoiningDate)
                        VALUES (?,?,?,?,?,?)`

    db.pool.query(statement,[emp_name , emp_Address ,emp_Email,emp_MobileNo,emp_DOB, emp_JoiningDate],(error , data)=>{
      
        response.send(utils.createResult(error,data))
    })

})

router.put('/updateemp',(request,response)=>{

    const{ emp_name , emp_Address ,emp_Email,emp_MobileNo,emp_DOB, emp_JoiningDate}=request.body

    const statement = `UPDATE employee SET 
                        emp_Address =?,
                        emp_Email =?,
                        emp_Mobileno =?,
                        emp_DOB =?,
                        emp_JoiningDate=?
                        WHERE emp_name =?`

    db.pool.query(statement,[ emp_Address ,emp_Email,emp_MobileNo,emp_DOB, emp_JoiningDate ,emp_name],(error , data)=>{
      
        response.send(utils.createResult(error,data))
    })

})

router.delete('/deleteemp',(request,response)=>{

    const{ emp_name}=request.body

    const statement = `DELETE FROM employee WHERE emp_name =?`

    db.pool.query(statement,[emp_name],(error , data)=>{
      
        response.send(utils.createResult(error,data))
    })

})
module.exports=router